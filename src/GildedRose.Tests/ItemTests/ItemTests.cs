﻿using GildedRose.Console.Entity;
using Xunit;

namespace GildedRose.Tests
{
    public class ItemTests
    {
        [Fact]
        public void TestTheTruth()
        {
            Assert.True(true);
        }

        [Fact]
        public void CheckingItemToStringMethodSyntax()
        {
            Item item = new Item();

            var expectedResult = item.Name + ", " + item.SellIn + ", " + item.Quality;
            var actualResult = item.ToString();

            Assert.Equal(expectedResult, actualResult);
        }
    }
}
