﻿using FluentAssertions;
using GildedRose.Console;
using GildedRose.Console.Constants;
using GildedRose.Console.Entity;
using GildedRose.Console.Enum;
using GildedRose.Console.Processors;
using System.Collections.Generic;
using Xunit;

namespace GildedRose.Tests
{
    public class ItemProcessorTests
    {
        [Fact]
        public void GetCategoryFromItemName()
        {
            ItemProcessor.GetItemCategory(new Item { Name = NameConstants.Brie }).Should().Be(ItemCategory.Brie);
            ItemProcessor.GetItemCategory(new Item { Name = NameConstants.Vest }).Should().Be(ItemCategory.NormalItem);
            ItemProcessor.GetItemCategory(new Item { Name = NameConstants.Passes }).Should().Be(ItemCategory.BackstagePasses);
            ItemProcessor.GetItemCategory(new Item { Name = NameConstants.Sulfuras }).Should().Be(ItemCategory.LegendaryItem);
            ItemProcessor.GetItemCategory(new Item { Name = NameConstants.Conjured }).Should().Be(ItemCategory.ConjuredItem);

            ItemProcessor.GetItemCategory(new Item { Name = NameConstants.Elixir }).Should().Be(ItemCategory.NormalItem);
            ItemProcessor.GetItemCategory(new Item { Name = "Random Item" }).Should().Be(ItemCategory.NormalItem);
        }

        [Fact]
        public void CreateSpecificItemProcessorsBasedOnItemCategory()
        {
            ItemProcessor.GetInstanceForProcessor(new Item { Name = NameConstants.Brie }).Should().BeOfType<BrieProcessor>();
            ItemProcessor.GetInstanceForProcessor(new Item { Name = NameConstants.Passes }).Should().BeOfType<BackstagePassesProcessor>();
            ItemProcessor.GetInstanceForProcessor(new Item { Name = NameConstants.Sulfuras }).Should().BeOfType<LegendaryItemProcessor>();
            ItemProcessor.GetInstanceForProcessor(new Item { Name = NameConstants.Conjured }).Should().BeOfType<ConjuredItemProcessor>();
            ItemProcessor.GetInstanceForProcessor(new Item { Name = "Another random item" }).Should().BeOfType<NormalItemProcessor>();
        }

        [Theory]
        [MemberData(nameof(TestItemData))]
        public void BrieProcessorShouldOnlyProcessBrieItems(string name, int sellIn, int quality, int newSellIn, int newQuality)
        {
            var item = new Item() { Name = name };

            var processor = ItemProcessor.GetInstanceForProcessor(item);
            if(ItemProcessor.GetItemCategory(item) == ItemCategory.Brie)
            {
                processor.Should().BeOfType<BrieProcessor>();
            }
            else
            {
                processor.Should().NotBeOfType<BrieProcessor>();
            }
        }

        [Theory]
        [MemberData(nameof(TestItemData))]
        public void BackstagePassesProcessorShouldOnlyProcessBackstagePassesItems(string name, int sellIn, int quality, int newSellIn, int newQuality)
        {
            var item = new Item() { Name = name };

            var processor = ItemProcessor.GetInstanceForProcessor(item);
            if (ItemProcessor.GetItemCategory(item) == ItemCategory.BackstagePasses)
            {
                processor.Should().BeOfType<BackstagePassesProcessor>();
            }
            else
            {
                processor.Should().NotBeOfType<BackstagePassesProcessor>();
            }
        }

        [Theory]
        [MemberData(nameof(TestItemData))]
        public void LegendaryItemsProcessorShouldOnlyProcessLegendaryItems(string name, int sellIn, int quality, int newSellIn, int newQuality)
        {
            var item = new Item() { Name = name };

            var processor = ItemProcessor.GetInstanceForProcessor(item);
            if (ItemProcessor.GetItemCategory(item) == ItemCategory.LegendaryItem)
            {
                processor.Should().BeOfType<LegendaryItemProcessor>();
            }
            else
            {
                processor.Should().NotBeOfType<LegendaryItemProcessor>();
            }
        }

        [Theory]
        [MemberData(nameof(TestItemData))]
        public void NormalItemsProcessorShouldOnlyProcessNormalItems(string name, int sellIn, int quality, int newSellIn, int newQuality)
        {
            var item = new Item() { Name = name };

            var processor = ItemProcessor.GetInstanceForProcessor(item);
            if (ItemProcessor.GetItemCategory(item) == ItemCategory.NormalItem)
            {
                processor.Should().BeOfType<NormalItemProcessor>();
            }
            else
            {
                processor.Should().NotBeOfType<NormalItemProcessor>();
            }
        }

        [Theory]
        [MemberData(nameof(TestItemData))]
        public void ProcessItemsAsPrescribed(string name, int sellIn, int quality, int newSellIn, int newQuality)
        {
            var item = new Item { Name = name, Quality = quality, SellIn = sellIn };

            ItemProcessor.GetInstanceForProcessor(item).ProcessItem(item);

            item.Quality.Should().Be(newQuality);
            item.SellIn.Should().Be(newSellIn);
        }

        //Added detailed test for all cases prescribed
        public static IEnumerable<object[]> TestItemData =>
        new List<object[]>
        {
            new object[] { NameConstants.Vest, 10, 20, 9, 19},
            new object[] { NameConstants.Vest, 10, 0, 9, 0},
            new object[] { NameConstants.Vest, 0, 20, -1, 18},
            new object[] { NameConstants.Brie, 2, 0, 1, 1 },
            new object[] { NameConstants.Brie, 2, 50, 1, 50 },
            new object[] { NameConstants.Brie, 0, 0, -1, 2 },
            new object[] { NameConstants.Elixir, 5, 7, 4, 6 },
            new object[] { NameConstants.Sulfuras, 0, 80, 0, 80},
            new object[] { NameConstants.Sulfuras, -1, 80, -1, 80},
            new object[] { NameConstants.Passes, 15, 20, 14, 21},
            new object[] { NameConstants.Passes, 15, 50, 14, 50},
            new object[] { NameConstants.Passes, 10, 20, 9, 22},
            new object[] { NameConstants.Passes, 11, 20, 10, 21},
            new object[] { NameConstants.Passes, 5, 20, 4, 23},
            new object[] { NameConstants.Passes, 6, 20, 5, 22},
            new object[] { NameConstants.Passes, 6, 50, 5, 50},
            new object[] { NameConstants.Passes, 6, 49, 5, 50},
            new object[] { NameConstants.Passes, 0, 20, -1, 0},
            new object[] { NameConstants.Conjured, 3, 6, 2, 4},
            new object[] { NameConstants.Conjured, 10, 20, 9, 18},
            new object[] { NameConstants.Conjured, 10, 0, 9, 0},
            new object[] { NameConstants.Conjured, 0, 20, -1, 16},
        };
    }
}
