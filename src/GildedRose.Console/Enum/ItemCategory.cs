﻿namespace GildedRose.Console.Enum
{
    //Category enums are responsible for separating items by their fields.
    public enum ItemCategory
    {
        Brie,
        NormalItem, //default item
        BackstagePasses,
        LegendaryItem,
        ConjuredItem
    }
}
