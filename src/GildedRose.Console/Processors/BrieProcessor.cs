﻿using GildedRose.Console.Entity;

namespace GildedRose.Console.Processors
{
    public class BrieProcessor : ItemProcessor
    {
        public override void ProcessItem(Item item)
        {
            if (--item.SellIn < 0)
                item.Quality += 2;
            else
                item.Quality++;

            if(item.Quality > 50)
                item.Quality = 50;
        }
    }
}
