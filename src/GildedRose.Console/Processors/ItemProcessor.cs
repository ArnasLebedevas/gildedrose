﻿using GildedRose.Console.Constants;
using GildedRose.Console.Entity;
using GildedRose.Console.Enum;
using GildedRose.Console.Processors;
using System;

namespace GildedRose.Console
{
    public class ItemProcessor
    {
        public virtual void ProcessItem(Item item)
        {
            throw new Exception("Unknown item!");
        }

        public static ItemProcessor GetInstanceForProcessor(Item item)
        {
            switch (GetItemCategory(item))
            {
                case ItemCategory.Brie:
                    return new BrieProcessor();
                case ItemCategory.BackstagePasses:
                    return new BackstagePassesProcessor();
                case ItemCategory.LegendaryItem:
                    return new LegendaryItemProcessor();
                case ItemCategory.ConjuredItem:
                    return new ConjuredItemProcessor();
                case ItemCategory.NormalItem:
                    return new NormalItemProcessor();
                default:
                    return new ItemProcessor();
            }
        }

        public static ItemCategory GetItemCategory(Item item)
        {
            if (item.Name.Equals(NameConstants.Brie))
                return ItemCategory.Brie;
            else if (item.Name.Equals(NameConstants.Passes))
                return ItemCategory.BackstagePasses;
            else if (item.Name.Equals(NameConstants.Sulfuras))
                return ItemCategory.LegendaryItem;
            else if (item.Name.StartsWith(NameConstants.Conjured))
                return ItemCategory.ConjuredItem;
            else
                return ItemCategory.NormalItem;
        }
    }
}
