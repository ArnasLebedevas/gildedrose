﻿using GildedRose.Console.Entity;

namespace GildedRose.Console.Processors
{
    public class ConjuredItemProcessor : ItemProcessor
    {
        public override void ProcessItem(Item item)
        {
            if (--item.SellIn < 0)
                item.Quality -= 4;
            else
                item.Quality -= 2;

            if (item.Quality < 0)
                item.Quality = 0;
        }
    }
}
