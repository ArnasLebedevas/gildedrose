﻿namespace GildedRose.Console.Constants
{
    //NameConstants class responsible for string constants to use in all project.
    public class NameConstants
    {
        public const string Vest = "+5 Dexterity Vest";
        public const string Brie = "Aged Brie";
        public const string Elixir = "Elixir of the Mongoose";
        public const string Sulfuras = "Sulfuras, Hand of Ragnaros";
        public const string Passes = "Backstage passes to a TAFKAL80ETC concert";
        public const string Conjured = "Conjured";
    }
}
