﻿using System.Collections.Generic;

namespace GildedRose.Console.Entity
{
    //Item class responsible for item parameters and update method. 
    public class Item
    {
        private readonly IList<Item> Items;

        public Item()
        { }

        public Item(IList<Item> items)
        {
            Items = items;
        }

        public string Name { get; set; }

        public int SellIn { get; set; }

        public int Quality { get; set; }

        public override string ToString()
        {
            return Name + ", " + SellIn + ", " + Quality;
        }

        public void UpdateQuality()
        {
            foreach (Item item in Items)
            {
                ItemProcessor.GetInstanceForProcessor(item).ProcessItem(item);
            }
        }
    }
}
