﻿using GildedRose.Console.Entity;
using System.Collections.Generic;

namespace GildedRose.Console.Repository
{
    public class ItemRepository
    {
        public void GetItems()
        {
            var items = InitializeItemList();

            var item = new Item(items);

            item.UpdateQuality();

            DisplayItemListInConsole(items, item);
        }

        private void DisplayItemListInConsole(IList<Item> items, Item updateItem)
        {
            for (var i = 0; i < 31; i++)
            {
                System.Console.WriteLine(string.Format("-------- day {0} --------", i));

                foreach (Item item in items)
                {
                    System.Console.WriteLine(item);
                }

                System.Console.WriteLine(string.Empty);

                updateItem.UpdateQuality();
            }
        }

        private IList<Item> InitializeItemList()
        {
            IList<Item> Items = new List<Item>
            {
                 new Item {Name = "+5 Dexterity Vest", SellIn = 10, Quality = 20},
                 new Item {Name = "Aged Brie", SellIn = 2, Quality = 0},
                 new Item {Name = "Elixir of the Mongoose", SellIn = 5, Quality = 7},
                 new Item {Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80},
                 new Item {Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 15, Quality = 20},
                 new Item {Name = "Conjured Mana Cake", SellIn = 3, Quality = 6}
            };

            return Items;
        }
    }
}
