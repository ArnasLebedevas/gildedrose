﻿using GildedRose.Console.Repository;

namespace GildedRose.Console
{
    public class Program
    {
        // I Removed all code from program class, to make main method clean as possible without unnecessary code
        static void Main(string[] args)
        {
            var repository = new ItemRepository();

            repository.GetItems();
        }
    }
}
